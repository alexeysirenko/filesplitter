package com.sysgears.filesplitter.chunk;

import org.testng.annotations.Test;

import java.util.Iterator;

import static org.testng.Assert.*;

/**
 * ChunkSplittingCollection test case.
 */
public class ChunkSplittingCollectionTest {

    private final int TOTAL_SIZE = 1024;
    private final int CHUNK_SIZE = 500;

    @Test
    public void testSplit() throws Exception {
        ChunkSplittingCollection chunks = ChunkSplittingCollection.split(TOTAL_SIZE, CHUNK_SIZE);
        long totalSize = 0;
        Iterator<Chunk> iterator = chunks.iterator();
        for (int i = 0; i < chunks.size(); i++) {
            Chunk chunk = iterator.next();
            long expectedSize = Math.min(CHUNK_SIZE, TOTAL_SIZE - totalSize);
            assertEquals(chunk.getSize(), expectedSize);
            assertEquals(totalSize, chunk.getOffset());
            totalSize = totalSize + chunk.getSize();
        }
    }

    @Test
    public void testSize() throws Exception {
        ChunkSplittingCollection chunks = ChunkSplittingCollection.split(TOTAL_SIZE, CHUNK_SIZE);
        assertEquals(chunks.size(), TOTAL_SIZE / CHUNK_SIZE + (TOTAL_SIZE % CHUNK_SIZE > 0 ? 1 : 0));
    }
}