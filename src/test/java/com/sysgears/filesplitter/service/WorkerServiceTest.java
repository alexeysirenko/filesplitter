package com.sysgears.filesplitter.service;

import com.sysgears.filesplitter.constants.Constants;
import com.sysgears.filesplitter.filehelper.ChunkFileHepler;
import com.sysgears.filesplitter.monitoring.MonitorFactory;
import com.sysgears.filesplitter.parser.CommandlineParser;
import com.sysgears.filesplitter.threadpool.WorkerPoolFactory;
import com.sysgears.filesplitter.workerfactory.WorkerFactory;
import org.junit.Rule;
import org.junit.rules.Timeout;

import java.io.*;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * WorkerService test
 */
public class WorkerServiceTest {

    @Rule
    public Timeout globalTimeout = new Timeout(3000);

    public void testProcessCommand() throws Exception {
        final int SOURCE_SIZE = 5;
        final String SOURCE_FILENAME = System.getProperty("java.io.tmpdir") + File.separatorChar + "source.bin";

        File source = new File(SOURCE_FILENAME);
        source.delete();
        int j = Constants.CHUNK_INITIAL_ID;
        while (true) {
            File chunk = ChunkFileHepler.getChunkFile(source, j);
            if (!chunk.exists()) {
                break;
            }
            chunk.delete();
            j++;
        }
        Deque<String> commands = new ConcurrentLinkedDeque<>();
        WorkerService service = new WorkerService(commands, new WorkerPoolFactory(), new MonitorFactory(),
                new WorkerFactory(), new CommandlineParser());
        Thread serviceThread = new Thread(service);
        serviceThread.start();
        try {
            source = makeFile(SOURCE_FILENAME, SOURCE_SIZE * (long) Math.pow(1024, 2));
            String command = "s " + source.getCanonicalPath() + " 1M";
            commands.push(command);
            while (true) {
                boolean await = false;
                for (int i = Constants.CHUNK_INITIAL_ID; i <= SOURCE_SIZE; i++) {
                    File dest = ChunkFileHepler.getChunkFile(source, i);
                    if (!(dest.exists() && (dest.length() == (long) Math.pow(1024, 2)))) {
                        await = true;
                        break;
                    }
                }
                if (!await) {
                    break;
                }
            }
            source.delete();
            source = new File(SOURCE_FILENAME);
            command = "a " + source.getCanonicalPath();
            commands.push(command);
            while (true) {
                if (source.exists() && (source.length() == SOURCE_SIZE * (long) Math.pow(1024, 2))) {
                    break;
                }
            }
        } finally {
            serviceThread.interrupt();
        }
    }

    private File makeFile(String name, final long size) {
        File file = new File(name);
        try (
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))
        ) {
            for (long i = 0; i < size; i++) {
                outputStream.write(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}