package com.sysgears.filesplitter.constants;

/**
 * Constants holder.
 */
public abstract class Constants {

    /**
     * Prefix of the chunk file.
     */
    public static final String CHUNK_PREFIX = "part";

    /**
     * Id value of the first chunk to increment
     */
    public static final int CHUNK_INITIAL_ID = 1;
}
