package com.sysgears.filesplitter.parser;

import com.sysgears.filesplitter.workablecollection.builder.WorkableCollectionBuilder;
import com.sysgears.filesplitter.workablecollection.types.WorkableCollectionType;
import com.sysgears.filesplitter.units.SizeUnit;
import com.sysgears.filesplitter.exceptions.ParseException;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Command line parser.
 */
public class CommandlineParser {

    /**
     * Performs command line parsing and validation.
     *
     * @param command command line
     * @return builder instance
     * @throws ParseException in case of invalid command line
     */
    public WorkableCollectionBuilder parseCommand(final String command) throws ParseException {
        String[] args = command.split("\\s");
        if (args.length < 2) {
            throw new ParseException("Lack of command line arguments");
        }
        WorkableCollectionBuilder result;
        WorkableCollectionType workableCollectionType;
        try {
            workableCollectionType = WorkableCollectionType.getTypeByCommand(args[0]);
        } catch (IllegalArgumentException e) {
            throw new ParseException(String.format("Invalid command '%s'", args[0]));
        }
        result = new WorkableCollectionBuilder(workableCollectionType, parseFile(args[1]));
        if (workableCollectionType == WorkableCollectionType.SPLIT) {
            if (args.length > 2) {
                result.setChunkSize(parseFileSize(args[2]));
            } else {
                throw new ParseException("File size parameter is missing");
            }
        }
        return result;
    }

    /**
     * Extracts file from quoted command line argument.
     *
     * @param fileName file name, not null
     * @return File instance
     * @throws ParseException in case provided file name does not match the pattern
     */
    private File parseFile(final String fileName) throws ParseException {
        Matcher m = Pattern.compile("^\"?(.+)\"?$").matcher(fileName); // Extract unquoted file name
        if (!m.matches()) {
            throw new ParseException(String.format("Invalid source file %s", fileName));
        }
        return new File(m.group(1));
    }

    /**
     * Resolve chunk size and convert it to bytes.
     *
     * @param fileSize file size, not null
     * @return size of single chunk in bytes
     * @throws ParseException in case of invalid chunk size
     */
    private long parseFileSize(final String fileSize) throws ParseException {
        Matcher m = Pattern.compile("^([0-9]+\\.?[0-9]*)(\\w?)$").matcher(fileSize); // Is a number with unit
        if (!m.matches()) {
            throw new ParseException(String.format("Invalid file size argument %s", fileSize));
    }
        double size;
        try {
            size = Double.parseDouble(m.group(1));
            if (!m.group(2).isEmpty()) {
                SizeUnit sizeUnit = SizeUnit.getUnitByName(m.group(2));
                size = sizeUnit.toBytes(size);
            }
        } catch (IllegalArgumentException e) {
            throw new ParseException(e);
        }
        if (!(size > 0)) {
            throw new ParseException("File size must be a positive number");
        }
        return Math.round(size);
    }
}
