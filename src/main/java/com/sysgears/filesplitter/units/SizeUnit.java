package com.sysgears.filesplitter.units;

/**
 * File size units enum. Can provide conversion of file size in specific units to plain bytes
 */
public enum SizeUnit {
    KILOBYTES("K", 1),
    MEGABYTES("M", 2),
    GIGABYTES("G", 3),
    TERABYTES("T", 4)
    ;

    /**
     * SizeUnit exponent.
     */
    private final int exponent;

    /**
     * SizeUnit abbreviation.
     */
    private final String abbreviation;

    /**
     * Creates enum instance.
     *
     * @param abbreviation unit abbreviation
     * @param exponent unit exponent
     */
    SizeUnit(final String abbreviation, final int exponent) {
        this.abbreviation = abbreviation;
        this.exponent = exponent;
    }

    /**
     * Converts given size to bytes.
     *
     * @param size size in other units
     * @return size in bytes
     */
    public long toBytes(final double size) {
        return Math.round(size * Math.pow(1024, exponent));
    }

    /**
     * Resolves unit by its name.
     * <p>
     * Case-insensitive.
     *
     * @param name unit name
     * @return SizeUnit instance
     * @throws IllegalArgumentException in case of unit name does not match any instance
     */
    public static SizeUnit getUnitByName(final String name) throws IllegalArgumentException {
        for (SizeUnit sizeUnit : SizeUnit.values()) {
            if (sizeUnit.abbreviation.equalsIgnoreCase(name)) {
                return sizeUnit;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid unit name '%s'", name));
    }

}
