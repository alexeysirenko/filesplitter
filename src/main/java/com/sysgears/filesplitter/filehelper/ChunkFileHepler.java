package com.sysgears.filesplitter.filehelper;

import com.sysgears.filesplitter.constants.Constants;

import java.io.File;

/**
 * Chunk file helper class.
 */
public abstract class ChunkFileHepler {

    /**
     * Returns chunk file name string based on source file name and it's id using default formatting
     *
     * @param source source file
     * @param chunkId chunk id
     * @return chunk file
     */
    public static File getChunkFile(File source, int chunkId) {
        return new File(String.format("%s.%s%d", source, Constants.CHUNK_PREFIX, chunkId));
    }
}
