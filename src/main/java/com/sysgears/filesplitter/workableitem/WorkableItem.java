package com.sysgears.filesplitter.workableitem;

import java.io.File;

/**
 * Workable item class to be processed by worker. Contains information about a work to be
 * done by single thread, such as input and output files and size of a block to be copied.
 */
public class WorkableItem {

    /**
     * File to read from.
     */
    private final File input;

    /**
     * File to write.
     */
    private final File output;

    /**
     * Input file reading offset.
     */
    private final long inputOffset;

    /**
     * Output file writing offset.
     */
    private final long outputOffset;

    /**
     * Size of a fiel block to read and write.
     */
    private final long size;

    /**
     * Creates class instance
     *
     * @param input input file
     * @param output output file.
     * @param inputOffset input file offset.
     * @param outputOffset output file offset.
     * @param size size of an file area to be copied.
     */
    public WorkableItem(File input, long inputOffset, File output, long outputOffset, long size) {
        this.input = input;
        this.inputOffset = inputOffset;
        this.output = output;
        this.outputOffset = outputOffset;
        this.size = size;
    }

    /**
     * Returns input file.
     *
     * @return input file
     */
    public File getInput() {
        return input;
    }

    /**
     * Returns output file.
     *
     * @return output file
     */
    public File getOutput() {
        return output;
    }

    /**
     * Returns input file reading offset.
     *
     * @return input file reading offset
     */
    public long getInputOffset() {
        return inputOffset;
    }

    /**
     * Returns output file writing offset.
     *
     * @return output file writing offset
     */
    public long getOutputOffset() {
        return outputOffset;
    }

    /**
     * Returns size of an area to be copied from input to output file.
     *
     * @return size of an area to be copied from input to output file
     */
    public long getSize() {
        return size;
    }

    /**
     * String object representation.
     *
     * @return string object representation
     */
    @Override
    public String toString() {
        return String.format("'%s' [%d] -> '%s' [%d] (size %d)", input, inputOffset, output, outputOffset, size);
    }
}
