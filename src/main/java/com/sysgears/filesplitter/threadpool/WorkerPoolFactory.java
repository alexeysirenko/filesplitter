package com.sysgears.filesplitter.threadpool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Factory to create thread pool.
 */
public class WorkerPoolFactory {

    /**
     * Creates thread pool of fixed size.
     *
     * @param size maximum pool size
     * @return new instance of a thread pool
     */
    public ThreadPoolExecutor create(final int size) {
        return new ThreadPoolExecutor(size, size, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    }
}
