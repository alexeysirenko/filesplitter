package com.sysgears.filesplitter.exceptions;

/**
 * Exception during parsing of initial expression string.
 */
public class ParseException extends Exception {

    /**
     * Creates instance of the exception.
     *
     * @param e parent exception
     */
    public ParseException(Exception e) {
        super(e);
    }

    /**
     * Creates instance of the exception.
     *
     * @param message error message
     */
    public ParseException(String message) {
        super(message);
    }
}
