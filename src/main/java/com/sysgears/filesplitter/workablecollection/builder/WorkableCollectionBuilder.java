package com.sysgears.filesplitter.workablecollection.builder;

import com.sysgears.filesplitter.workablecollection.types.WorkableCollectionType;

import java.io.File;

/**
 * Task builder class.
 */
public class WorkableCollectionBuilder {

    /**
     * Source file.
     */
    private final File sourceFile;

    /**
     * Job types.
     */
    private final WorkableCollectionType workableCollectionType;

    /**
     * Size of a single file chunk.
     */
    private long chunkSize = 0;

    /**
     * Creates builder instance.
     *
     * @param sourceFile source file
     */
    public WorkableCollectionBuilder(WorkableCollectionType workableCollectionType, File sourceFile) {
        this.sourceFile = sourceFile;
        this.workableCollectionType = workableCollectionType;
    }

    /**
     * Sets size of a single chunk.
     *
     * @param chunkSize size of a single chunk
     * @return current instance
     */
    public WorkableCollectionBuilder setChunkSize(final long chunkSize) {
        this.chunkSize = chunkSize;
        return this;
    }

    /**
     * Returns source file.
     *
     * @return source file
     */
    public File getSourceFile() {
        return sourceFile;
    }

    /**
     * Returns job types.
     *
     * @return job types
     */
    public WorkableCollectionType getWorkableCollectionType() {
        return workableCollectionType;
    }

    /**
     * Returns chunk size.
     *
     * @return chunk size
     */
    public long getChunkSize() {
        return chunkSize;
    }
}
