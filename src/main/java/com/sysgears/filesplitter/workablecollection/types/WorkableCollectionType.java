package com.sysgears.filesplitter.workablecollection.types;

import com.sysgears.filesplitter.workablecollection.AbstractWorkableCollection;
import com.sysgears.filesplitter.workablecollection.builder.WorkableCollectionBuilder;
import com.sysgears.filesplitter.workablecollection.custom.AssembleWorkableCollection;
import com.sysgears.filesplitter.workablecollection.custom.SplitWorkableCollection;

/**
 * Enumerates all possible jobs could be done by program with command line reference.
 */
public enum WorkableCollectionType {
    SPLIT("SPLIT", "s") {

        /**
         * Returns associated workable collection.
         *
         * @param builder workable collection builder, not null.
         * @return workable collection instance
         */
        @Override
        public AbstractWorkableCollection createWorkableCollectionOf(WorkableCollectionBuilder builder) {
            return SplitWorkableCollection.createCollectionOf(builder);
        }

    },
    ASSEMBLE("ASSEMBLE", "a") {

        /**
         * Returns associated workable collection.
         *
         * @param builder workable collection builder, not null.
         * @return workable collection instance
         */
        @Override
        public AbstractWorkableCollection createWorkableCollectionOf(WorkableCollectionBuilder builder) {
            return AssembleWorkableCollection.createCollectionOf(builder);
        }
    }
    ;

    /**
     * Job name.
     */
    private final String name;

    /**
     * Job command line argument.
     */
    private final String command;

    /**
     * Creates job types instance.
     *
     * @param name job name
     */
    WorkableCollectionType(String name, String command) {
        this.name = name;
        this.command = command;
    }

    /**
     * Resolves collection types by its command name.
     * <p>
     * Case-insensitive.
     *
     * @param commandName command name
     * @return WorkableCollectionType instance
     * @throws IllegalArgumentException in case of collection command name does not match any instance
     */
    public static WorkableCollectionType getTypeByCommand(final String commandName) throws IllegalArgumentException {
        for (WorkableCollectionType workableCollectionType : WorkableCollectionType.values()) {
            if (workableCollectionType.command.equalsIgnoreCase(commandName)) {
                return workableCollectionType;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid command name '%s'", commandName));
    }

    /**
     * Returns associated workable collection.
     *
     * @param builder workable collection builder, not null.
     * @return workable collection instance
     */
    public abstract AbstractWorkableCollection createWorkableCollectionOf(WorkableCollectionBuilder builder);

    /**
     * Returns string object representation.
     *
     * @return string object representation
     */
    @Override
    public String toString() {
        return name;
    }
}
