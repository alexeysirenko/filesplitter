package com.sysgears.filesplitter.workablecollection;

import com.sysgears.filesplitter.workablecollection.builder.WorkableCollectionBuilder;
import com.sysgears.filesplitter.workableitem.WorkableItem;


/**
 * Abstract WorkableCollection class. Creates collection of workable items to be processed by worker.
 */
public abstract class AbstractWorkableCollection implements Iterable<WorkableItem> {

    /**
     * WorkableCollection builder.
     */
    protected final WorkableCollectionBuilder builder;

    /**
     * Creates class instance.
     *
     * @param builder collection builder, not null
     * @return workablecollection instance
     */
    protected AbstractWorkableCollection(WorkableCollectionBuilder builder) {
        this.builder = builder;
    }

    /**
     * Total amount of work required to be done to process collection of workable items.
     *
     * @return total work amount.
     */
    public abstract long totalWorkAmount();

}
