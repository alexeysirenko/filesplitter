package com.sysgears.filesplitter.workablecollection.custom;

import com.sysgears.filesplitter.filehelper.ChunkFileHepler;
import com.sysgears.filesplitter.constants.Constants;
import com.sysgears.filesplitter.workablecollection.AbstractWorkableCollection;
import com.sysgears.filesplitter.workablecollection.builder.WorkableCollectionBuilder;
import com.sysgears.filesplitter.workableitem.WorkableItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Assemble workable collection class. Provides collection of workable items to assemble source file from a chunks.
 */
public class AssembleWorkableCollection extends AbstractWorkableCollection {

    /**
     * Total size of all chunk files.
     */
    private long totalChunksSize = 0;

    /**
     * List of all chunk files of a source file.
     */
    private final List<File> chunkFiles = new ArrayList<>();

    /**
     * Creates class instance.
     *
     * @param builder workable collection builder, not null
     */
    protected AssembleWorkableCollection(WorkableCollectionBuilder builder) {
        super(builder);
        loadChunkFiles(builder.getSourceFile());
    }

    /**
     * Creates class instance.
     *
     * @param builder workable collection builder, not null
     * @return AssembleWorkableCollection instance, not null
     */
    public static AssembleWorkableCollection createCollectionOf(WorkableCollectionBuilder builder) {
        return new AssembleWorkableCollection(builder);
    }

    /**
     * Loads list of all chunk files belong to a source file and calculates their total size.
     *
     * @param source source file.
     */
    private void loadChunkFiles(File source) {
        int chunkId = Constants.CHUNK_INITIAL_ID;
        while (true) {
            File chunkFile = ChunkFileHepler.getChunkFile(source, chunkId);
            if (!chunkFile.exists()) {
                break;
            }
            chunkFiles.add(chunkFile);
            totalChunksSize = totalChunksSize + chunkFile.length();
            chunkId++;
        }
    }

    /**
     * Total amount of work required to be done to process collection of workable items.
     *
     * @return total work amount.
     */
    public long totalWorkAmount() {
        return totalChunksSize;
    }

    @Override
    public Iterator<WorkableItem> iterator() {
        return new Iterator<WorkableItem>() {

            /**
             * Chunk files list cursor.
             */
            private int cursor = 0;

            /**
             * Total size of chunks found.
             */
            private long totalSize = 0;

            /**
             * Returns true if the iteration has more elements. In other words, returns true if next()
             * would return an element rather than throwing an exception.
             *
             * @return true if the iteration has more elements
             */
            @Override
            public boolean hasNext() {
                return cursor < AssembleWorkableCollection.this.chunkFiles.size();
            }

            /**
             * Returns the next element in the iteration.
             *
             * @return the next element in the iteration
             */
            @Override
            public WorkableItem next() {
                WorkableItem result;
                if (!hasNext()) {
                    throw new NoSuchElementException("No such element");
                }
                File chunkFile = AssembleWorkableCollection.this.chunkFiles.get(cursor);
                result = new WorkableItem(chunkFile, 0, builder.getSourceFile(), totalSize, chunkFile.length());
                totalSize = totalSize + chunkFile.length();
                cursor++;
                return result;
            }

            /**
             * Not implemented. Removes from the underlying collection the last element returned by this iterator.             *
             */
            @Override
            public void remove() {
                throw new UnsupportedOperationException("Remove is not implemented");
            }
        };
    }
}
