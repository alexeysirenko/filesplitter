package com.sysgears.filesplitter.workablecollection.custom;

import com.sysgears.filesplitter.chunk.Chunk;
import com.sysgears.filesplitter.chunk.ChunkSplittingCollection;
import com.sysgears.filesplitter.filehelper.ChunkFileHepler;
import com.sysgears.filesplitter.constants.Constants;
import com.sysgears.filesplitter.workablecollection.AbstractWorkableCollection;
import com.sysgears.filesplitter.workablecollection.builder.WorkableCollectionBuilder;
import com.sysgears.filesplitter.workableitem.WorkableItem;

import java.io.File;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Split file workablecollection. Provides collection of workable items to split source file to chunks of provided size.
 */
public class SplitWorkableCollection extends AbstractWorkableCollection {

    /**
     * Chunks collection.
     */
    private final ChunkSplittingCollection chunks;

    /**
     * Creates class instance.
     *
     * @param builder workable collection builder, not null
     */
    protected SplitWorkableCollection(WorkableCollectionBuilder builder) {
        super(builder);
        chunks = ChunkSplittingCollection.split(builder.getSourceFile().length(), builder.getChunkSize());
    }

    /**
     * Creates class instance.
     *
     * @param builder workable collection builder, not null
     * @return AssembleWorkableCollection instance, not null
     */
    public static SplitWorkableCollection createCollectionOf(WorkableCollectionBuilder builder) {
        return new SplitWorkableCollection(builder);
    }

    /**
     * Total amount of work required to be done to process collection of workable items.
     *
     * @return total work amount.
     */
    public long totalWorkAmount() {
        return builder.getSourceFile().length();
    }

    @Override
    public Iterator<WorkableItem> iterator() {
        return new Iterator<WorkableItem>() {

            private final Iterator<Chunk> iterator = chunks.iterator();

            private int chunkId = Constants.CHUNK_INITIAL_ID;

            /**
             * Returns true if the iteration has more elements. In other words, returns true if next()
             * would return an element rather than throwing an exception.
             *
             * @return true if the iteration has more elements
             */
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            /**
             * Returns the next element in the iteration.
             *
             * @return the next element in the iteration
             */
            @Override
            public WorkableItem next() {
                WorkableItem result;
                if (!hasNext()) {
                    throw new NoSuchElementException("No such element");
                }
                Chunk chunk = iterator.next();
                File chunkFile = ChunkFileHepler.getChunkFile(builder.getSourceFile(), chunkId);
                result = new WorkableItem(builder.getSourceFile(), chunk.getOffset(), chunkFile, 0, chunk.getSize());
                chunkId++;
                return result;
            }

            /**
             * Not implemented. Removes from the underlying collection the last element returned by this iterator.             *
             */
            @Override
            public void remove() {
                throw new UnsupportedOperationException("Remove is not implemented");
            }
        };
    }
}
