package com.sysgears.filesplitter.chunk;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Chunks collection. Creates instance using known total size and size of a single chunk.
 * The iterator passes through the total size and splits it to chunks with distinct offset.
 * All the chunks will have size equivalent to provided chunkSize or lesser in case of the
 * last chunk (which will contain only amount of total size left)
 */
public class ChunkSplittingCollection implements Iterable<Chunk> {

    /**
     * Collection total size
     */
    private final long totalSize;

    /**
     * Single chunk size
     */
    private final long chunkSize;

    /**
     * Creates class instance
     *
     * @param totalSize total size to be chunked
     * @param chunkSize size of a chunk
     */
    private ChunkSplittingCollection(final long totalSize, final long chunkSize) {
        this.totalSize = totalSize;
        this.chunkSize = chunkSize;
    }

    /**
     * Creates class instance
     *
     * @param totalSize total size to be chunked
     * @param chunkSize size of a chunk
     */
    public static ChunkSplittingCollection split(final long totalSize, final long chunkSize) {
        return new ChunkSplittingCollection(totalSize, chunkSize);
    }

    /**
     * Returns size of the collection.
     *
     * @return size of the collection
     */
    public long size() {
        return totalSize / chunkSize + (totalSize % chunkSize > 0 ? 1 : 0);
    }

    /**
     * Chunk collection iterator. Creates a new Chunk instance of next method call
     *
     * @return iterator
     */
    @Override
    public Iterator<Chunk> iterator() {
        return new Iterator<Chunk>() {

            /**
             * Cursor
             */
            private long cursor = 0;

            /**
             * Returns true if the iteration has more elements. In other words, returns true if next()
             * would return an element rather than throwing an exception.
             *
             * @return true if the iteration has more elements
             */
            @Override
            public boolean hasNext() {
                return cursor < ChunkSplittingCollection.this.totalSize;
            }

            /**
             * Returns the next element in the iteration.
             *
             * @return the next element in the iteration
             */
            @Override
            public Chunk next() {
                if (!hasNext()) {
                    throw new NoSuchElementException("No such element");
                }
                long size = Math.min(ChunkSplittingCollection.this.chunkSize, ChunkSplittingCollection.this.totalSize - cursor);
                Chunk result = new Chunk(cursor, size);
                cursor = cursor + size;
                return result;
            }

            /**
             * Not implemented. Removes from the underlying collection the last element returned by this iterator.             *
             */
            @Override
            public void remove() {
                throw new UnsupportedOperationException("Remove is not implemented");
            }
        };
    }
}
