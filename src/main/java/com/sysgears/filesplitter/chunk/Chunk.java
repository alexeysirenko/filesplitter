package com.sysgears.filesplitter.chunk;

/**
 * Chunk, a piece of the some single unit with specific offset and size.
 */
public class Chunk {

    /**
     * Chunk offset.
     */
    private final long offset;

    /**
     * Chunk size.
     */
    private final long size;

    /**
     * Creates chunk instance.
     *
     * @param offset chunk offset
     * @param size chunk size
     */
    public Chunk(final long offset, final long size) {
        this.offset = offset;
        this.size = size;
    }

    /**
     * Returns offset.
     *
     * @return chunk offset
     */
    public long getOffset() {
        return offset;
    }

    /**
     * Returns size.
     *
     * @return chunk size
     */
    public long getSize() {
        return size;
    }

    @Override
    public String toString() {
        return String.format("Chunk %d:%d", offset, size);
    }
}
