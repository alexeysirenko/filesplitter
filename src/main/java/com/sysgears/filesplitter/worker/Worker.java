package com.sysgears.filesplitter.worker;

import com.sysgears.filesplitter.chunk.Chunk;
import com.sysgears.filesplitter.chunk.ChunkSplittingCollection;
import com.sysgears.filesplitter.monitoring.IWorkMonitor;
import com.sysgears.filesplitter.workableitem.WorkableItem;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.logging.*;


/**
 * Worker runnable. Executes provided WorkableItem instance
 */
public class Worker implements Runnable {

    /**
     * Read buffer size.
     */
    private final int BUFFER_SIZE = 8192;

    /**
     * Work item instance need to be executed
     */
    private final WorkableItem workableItem;

    /**
     * Work monitor.
     */
    private final IWorkMonitor monitor;

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(Worker.class.getName());

    /**
     * Creates worker instance. Requires workable item to process and monitor to track progress.
     *
     * @param workableItem workable item to process, not null
     * @param monitor work monitor, not null
     */
    public Worker(WorkableItem workableItem, IWorkMonitor monitor) {
        this.workableItem = workableItem;
        this.monitor = monitor;
    }

    /**
     * Run method implementation. Copies specified by workable item area of an input file to the
     * specific area of an output file.
     */
    public void run() {
        log.log(Level.FINEST, "Work started. Processing " + workableItem);
        try (
                RandomAccessFile inputFile = new RandomAccessFile(workableItem.getInput(), "rw");
                RandomAccessFile outputFile = new RandomAccessFile(workableItem.getOutput(), "rw");
                FileChannel readChannel = inputFile.getChannel();
                FileChannel writeChannel = outputFile.getChannel();
                FileLock inputLock = readChannel.lock(workableItem.getInputOffset(),workableItem.getSize(), true);
                FileLock outputLock = writeChannel.lock(workableItem.getOutputOffset(), workableItem.getSize(), true);

        ) {
            log.log(Level.FINEST, "Buffer allocation");
            monitor.progress(0); // Work start
            MappedByteBuffer mappedOutputBuffer = writeChannel.map(FileChannel.MapMode.READ_WRITE,
                    workableItem.getOutputOffset(), workableItem.getSize());
            inputFile.seek(workableItem.getInputOffset());
            byte[] buffer = new byte[BUFFER_SIZE];
            // Splitting provided for reading piece of the source file to the chunks equivalent to the buffer size,
            // than enumerate them, read each chunk of the source and write to the output
            log.log(Level.FINEST, String.format("Copying %s", workableItem));
            for (Chunk bufferChunk : ChunkSplittingCollection.split(workableItem.getSize(), BUFFER_SIZE)) {
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                int bytesRead = inputFile.read(buffer, 0, (int) bufferChunk.getSize());
                if (bytesRead != bufferChunk.getSize()) {
                    throw new IOException("Unexpected end of the file");
                }
                mappedOutputBuffer.put(buffer, 0, bytesRead);
                monitor.progress(bytesRead); // Bytes written since the last progress method call
            }
            log.log(Level.FINEST, "Copying is done");
        } catch (IOException | OverlappingFileLockException e) {
            log.log(Level.CONFIG, e.getMessage(), e);
        } catch (Throwable e) {
            log.log(Level.SEVERE, "Unexpected exception", e);
        }
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%s workableItem [%s] monitor [%s]", this.getClass().getName(), workableItem, monitor);
    }
}
