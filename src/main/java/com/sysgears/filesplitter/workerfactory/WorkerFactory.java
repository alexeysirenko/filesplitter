package com.sysgears.filesplitter.workerfactory;

import com.sysgears.filesplitter.monitoring.IWorkMonitor;
import com.sysgears.filesplitter.worker.Worker;
import com.sysgears.filesplitter.workableitem.WorkableItem;

/**
 * Splitter runnable factory.
 */
public class WorkerFactory {

    /**
     * Creates instance of worker runnable to execute provided work item
     *
     * @param workableItem work item to be executed, not null
     * @param monitor work monitor
     * @return new worker instance
     */
    public Worker create(WorkableItem workableItem, IWorkMonitor monitor) {
        return new Worker(workableItem, monitor);
    }
}
