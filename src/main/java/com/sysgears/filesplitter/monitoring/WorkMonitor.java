package com.sysgears.filesplitter.monitoring;

/**
 * Class to keep monitoring statistics and resolve parameters based on it
 * such as average speed and time left
 * <p>
 * Should be created for each workablecollection. Thread-safe - all public methods are synchronized.
 */
public class WorkMonitor implements IWorkMonitor {

    /**
     * Total amount of work to be done.
     */
    private final long total;

    /**
     * Progress of work done
     */
    private long done = 0;

    /**
     * Time when the work have been started.
     */
    private long startTime = 0;

    /**
     * Time when last worker have saved it's done.
     */
    private long lastAccessTime = 0;

    /**
     * Creates history monitorWorker instance for worker workablecollection.
     *
     * @param total total amount of work to be done
     */
    public WorkMonitor(long total) {
        this.total = total;
    }

    /**
     * Saves amount of work done since the last method call by a single worker.
     *
     * @param workDone done done by a singe worker
     */
    @Override
    public synchronized void progress(long workDone) {
        lastAccessTime = System.currentTimeMillis();
        if (startTime == 0) {
            startTime = lastAccessTime;
        }
        this.done = this.done + workDone;
    }

    /**
     * Returns total amount of work to be done.
     *
     * @return total amount of work to be done
     */
    @Override
    public synchronized long getTotal() {
        return total;
    }

    /**
     * Returns amount of done done in percents
     *
     * @return amount of done done in percents
     */
    @Override
    public synchronized long getProgress() {
        return (long) ((double) done / total * 100);
    }

    /**
     * Returns time left in milliseconds required to complete operation by all workers.
     *
     * @return time left in milliseconds required to complete operation by all workers
     */
    @Override
    public synchronized long getTimeLeft() {
        return getSpeed() > 0 ? (long) ((total - done) / getSpeed()) : 0;
    }

    /**
     * Returns estimated time in milliseconds
     *
     * @return estimated time in milliseconds
     */
    @Override
    public synchronized long getEstimatedTime() {
        return lastAccessTime - startTime;
    }

    /**
     * Returns current speed (per millisecond).
     *
     * @return current speed (per millisecond)
     */
    @Override
    public synchronized double getSpeed() {
        return (double) lastAccessTime > startTime ? done / (lastAccessTime - startTime) : 0;
    }

    @Override
    public synchronized String toString() {
        return String.format("Total: %d, Done: %d", total, done);
    }
}
