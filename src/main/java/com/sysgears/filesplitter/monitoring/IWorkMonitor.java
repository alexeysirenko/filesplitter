package com.sysgears.filesplitter.monitoring;

/**
 * Worker monitoring interface. Saves worker monitoring and allows to retrieve values based on statistics
 * such as monitoring speed and estimated time
 */
public interface IWorkMonitor {

    /**
     * Saves amount of work done by a single worker since the last method call
     *
     * @param workDone progress done by a singe worker
     */
    void progress(long workDone);

    /**
     * Total amount of work to be done.
     *
     * @return total amount of work to be done
     */
    long getTotal();

    /**
     * Returns work progress.
     *
     * @return work progress
     */
    long getProgress();

    /**
     * Returns time left in milliseconds required to complete operation by all workers.
     *
      * @return time left in milliseconds required to complete operation by all workers
     */
    long getTimeLeft();

    /**
     * Returns estimated time in milliseconds.
     *
     * @return estimated time in milliseconds.
     */
    long getEstimatedTime();

    /**
     * Returns speed of progress made by all workers (progress/milliseconds)
     *
     * @return speed of progress made by all workers.
     */
    double getSpeed();
}
