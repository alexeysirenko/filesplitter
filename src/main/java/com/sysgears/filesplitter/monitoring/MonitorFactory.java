package com.sysgears.filesplitter.monitoring;

/**
 * Monitor factory.
 */
public class MonitorFactory {

    /**
     * Creates monitor instance.
     *
     * @param total total amount of work to be done
     * @return monitor instance
     */
    public IWorkMonitor create(long total) {
        return new WorkMonitor(total);
    }
}
