package com.sysgears.filesplitter.service;

import com.sysgears.filesplitter.exceptions.ParseException;
import com.sysgears.filesplitter.monitoring.IWorkMonitor;
import com.sysgears.filesplitter.monitoring.MonitorFactory;
import com.sysgears.filesplitter.parser.CommandlineParser;
import com.sysgears.filesplitter.workablecollection.AbstractWorkableCollection;
import com.sysgears.filesplitter.workablecollection.builder.WorkableCollectionBuilder;
import com.sysgears.filesplitter.workablecollection.types.WorkableCollectionType;
import com.sysgears.filesplitter.workerfactory.WorkerFactory;
import com.sysgears.filesplitter.threadpool.WorkerPoolFactory;
import com.sysgears.filesplitter.workableitem.WorkableItem;

import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main worker service.
 */
public class WorkerService implements Runnable {

    /**
     * Number of threads running at the same time.
     */
    private final int THREAD_POOL_SIZE = 10;

    /**
     * Interval for displaying progress info
     */
    private final int PROGRESS_ECHO_INTERVAL = 50;

    /**
     * Thread pool factory.
     */
    private final WorkerPoolFactory poolFactory;

    /**
     * Monitor factory.
     */
    private final MonitorFactory monitorFactory;

    /**
     * Worker factory.
     */
    private final WorkerFactory workerFactory;

    /**
     * Parser.
     */
    private final CommandlineParser parser;

    /**
     * Command queue.
     */
    private final Deque<String> commandsQueue;

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(WorkerService.class.getName());

    /**
     * Creates service instance.
     *
     * @param commands commands list to be processed
     * @param poolFactory thread pool factory
     * @param monitorFactory work monitor factory
     * @param workerFactory worker runnable factory
     * @param parser command parser
     */
    public WorkerService(Deque<String> commands, WorkerPoolFactory poolFactory, MonitorFactory monitorFactory,
                         WorkerFactory workerFactory, CommandlineParser parser) {
        this.poolFactory = poolFactory;
        this.monitorFactory = monitorFactory;
        this.workerFactory = workerFactory;
        this.parser = parser;
        this.commandsQueue = commands;
    }

    /**
     * Process command queue.
     */
    @Override
    public void run() {
        log.log(Level.FINEST, "Service started");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (commandsQueue.size() > 0) {
                    processCommand(commandsQueue.pop());
                } else {
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        log.log(Level.FINEST, "Service stopped");
    }

    /**
     * Processes command and splits file to specified chunks.
     *
     * @param command command line with file name and chunk sizes
     */
    private void processCommand(final String command) {
        log.log(Level.FINEST, String.format("Processing command '%s'", command));
        try {
            WorkableCollectionBuilder builder = parser.parseCommand(command);
            WorkableCollectionType work = builder.getWorkableCollectionType();
            AbstractWorkableCollection workableCollection = work.createWorkableCollectionOf(builder);
            IWorkMonitor monitor = monitorFactory.create(workableCollection.totalWorkAmount());
            Iterator<WorkableItem> iterator = workableCollection.iterator();
            ThreadPoolExecutor pool = poolFactory.create(10);
            long echoTime = 0;
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    log.log(Level.FINEST, "Command processing has been interrupted. Shutting down child threads...");
                    pool.shutdownNow();
                    break;
                }
                if (iterator.hasNext() && (pool.getActiveCount() < THREAD_POOL_SIZE)) {
                    pool.execute(workerFactory.create(iterator.next(), monitor));
                } else {
                    if (!iterator.hasNext()) {
                        pool.shutdown();
                    }
                    Thread.sleep(20);
                }
                if (pool.isTerminated()) {
                    break;
                }
                if (System.currentTimeMillis() - echoTime > PROGRESS_ECHO_INTERVAL) {
                    echoProgress(monitor);
                    echoTime = System.currentTimeMillis();
                }
            }
            echoProgressDone(monitor);
            log.log(Level.FINEST, "Processing is done");
        } catch (ParseException e) {
            log.log(Level.SEVERE, "Command parsing exception", e);
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Throwable e) {
            log.log(Level.SEVERE, "Unexpected exception", e);
        }
    }

    /**
     * Prints process statistics into console
     *
     * @param monitor work monitor
     */
    private void echoProgress(IWorkMonitor monitor) {
        System.out.println(String.format("Progress: %d%% Speed %d MB/s Time left %d ms",
                monitor.getProgress(), (long) (monitor.getSpeed() * 1000 / Math.pow(1024, 2)),
                monitor.getTimeLeft()));
    }

    /**
     * Prints final process information on job done into console
     *
     * @param monitor work monitor
     */
    private void echoProgressDone(IWorkMonitor monitor) {
        System.out.println(String.format("Done in %d ms", monitor.getEstimatedTime()));
    }
}

















