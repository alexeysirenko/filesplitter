package com.sysgears.filesplitter;

import com.sysgears.filesplitter.monitoring.MonitorFactory;
import com.sysgears.filesplitter.parser.CommandlineParser;
import com.sysgears.filesplitter.service.WorkerService;
import com.sysgears.filesplitter.threadpool.WorkerPoolFactory;
import com.sysgears.filesplitter.workerfactory.WorkerFactory;

import java.util.Deque;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main application class.
 */
public class Application {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(Application.class.getName());

    /**
     * Application entry point.
     */
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            Deque<String> commands = new ConcurrentLinkedDeque<>();
            WorkerService service = new WorkerService(commands, new WorkerPoolFactory(), new MonitorFactory(),
                    new WorkerFactory(), new CommandlineParser());
            Thread serviceThread = new Thread(service);
            serviceThread.start();
            try {
                while (true) {
                    System.out.println(">");
                    System.out.println("Press Enter to exit");
                    System.out.println("Input file name and chunks to split on");
                    System.out.println("Command:");
                    String command = scanner.nextLine();
                    if (command.isEmpty()) {
                        return;
                    }
                    commands.push(command);
                }
            } finally {
                System.out.println("Shutting down service...");
                serviceThread.interrupt();
            }
        } catch (Throwable e) {
            log.log(Level.SEVERE, "Unexpected exception", e);
        }
    }
}
